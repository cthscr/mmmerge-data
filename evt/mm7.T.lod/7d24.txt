#1:0
#  Hint 100
05 01 00 00 04 64
#1:0
#  OnLoadMap
05 01 00 00 25 00
#1:1
#  Cmp QBits 611
0B 01 00 01 0E 10 00 63 02 00 00 04
#1:2
#  SetMonGroupBit 56 0x80000 1
0D 01 00 02 39 38 00 00 00 00 00 08 00 01
#1:3
#  Exit
05 01 00 03 01 00
#1:4
#  Exit
05 01 00 04 01 00
#3:0
#  Hint 1
05 03 00 00 04 01
#3:0
#  SetDoorState 1 0
06 03 00 00 0F 01 00
#3:1
#  SetDoorState 2 0
06 03 00 01 0F 02 00
#4:0
#  Hint 1
05 04 00 00 04 01
#4:0
#  SetDoorState 3 0
06 04 00 00 0F 03 00
#4:1
#  SetDoorState 4 0
06 04 00 01 0F 04 00
#5:0
#  Hint 1
05 05 00 00 04 01
#5:0
#  SetDoorState 5 0
06 05 00 00 0F 05 00
#5:1
#  SetDoorState 6 0
06 05 00 01 0F 06 00
#6:0
#  Hint 1
05 06 00 00 04 01
#6:0
#  SetDoorState 7 0
06 06 00 00 0F 07 00
#6:1
#  SetDoorState 8 0
06 06 00 01 0F 08 00
#151:0
#  Hint 4
05 97 00 00 04 04
#151:0
#  SetDoorState 10 1
06 97 00 00 0F 0A 01
#152:0
#  Hint 4
05 98 00 00 04 04
#152:0
#  SetDoorState 10 0
06 98 00 00 0F 0A 00
#153:0
#  Hint 4
05 99 00 00 04 04
#153:0
#  SetDoorState 11 1
06 99 00 00 0F 0B 01
#154:0
#  Hint 4
05 9A 00 00 04 04
#154:0
#  SetDoorState 11 0
06 9A 00 00 0F 0B 00
#176:0
#  Hint 7
05 B0 00 00 04 07
#176:0
#  OpenChest 0
05 B0 00 00 07 00
#176:1
#  Exit
05 B0 00 01 01 00
#177:0
#  Hint 7
05 B1 00 00 04 07
#177:0
#  OpenChest 1
05 B1 00 00 07 01
#177:1
#  Exit
05 B1 00 01 01 00
#178:0
#  Hint 7
05 B2 00 00 04 07
#178:0
#  OpenChest 2
05 B2 00 00 07 02
#178:1
#  Exit
05 B2 00 01 01 00
#179:0
#  Hint 7
05 B3 00 00 04 07
#179:0
#  OpenChest 3
05 B3 00 00 07 03
#179:1
#  Exit
05 B3 00 01 01 00
#180:0
#  Hint 7
05 B4 00 00 04 07
#180:0
#  OpenChest 4
05 B4 00 00 07 04
#180:1
#  Exit
05 B4 00 01 01 00
#181:0
#  Hint 7
05 B5 00 00 04 07
#181:0
#  OpenChest 5
05 B5 00 00 07 05
#181:1
#  Exit
05 B5 00 01 01 00
#182:0
#  Hint 7
05 B6 00 00 04 07
#182:0
#  OpenChest 6
05 B6 00 00 07 06
#182:1
#  Exit
05 B6 00 01 01 00
#183:0
#  Hint 7
05 B7 00 00 04 07
#183:0
#  OpenChest 7
05 B7 00 00 07 07
#183:1
#  Exit
05 B7 00 01 01 00
#184:0
#  Hint 3
05 B8 00 00 04 03
#184:0
#  OpenChest 8
05 B8 00 00 07 08
#184:1
#  Exit
05 B8 00 01 01 00
#185:0
#  Hint 3
05 B9 00 00 04 03
#185:0
#  OpenChest 9
05 B9 00 00 07 09
#185:1
#  Exit
05 B9 00 01 01 00
#186:0
#  Hint 3
05 BA 00 00 04 03
#186:0
#  OpenChest 10
05 BA 00 00 07 0A
#186:1
#  Exit
05 BA 00 01 01 00
#187:0
#  Hint 3
05 BB 00 00 04 03
#187:0
#  OpenChest 11
05 BB 00 00 07 0B
#187:1
#  Exit
05 BB 00 01 01 00
#188:0
#  Hint 3
05 BC 00 00 04 03
#188:0
#  OpenChest 12
05 BC 00 00 07 0C
#188:1
#  Exit
05 BC 00 01 01 00
#189:0
#  Hint 3
05 BD 00 00 04 03
#189:0
#  OpenChest 13
05 BD 00 00 07 0D
#189:1
#  Exit
05 BD 00 01 01 00
#190:0
#  Hint 3
05 BE 00 00 04 03
#190:0
#  OpenChest 14
05 BE 00 00 07 0E
#190:1
#  Exit
05 BE 00 01 01 00
#191:0
#  Hint 3
05 BF 00 00 04 03
#191:0
#  OpenChest 15
05 BF 00 00 07 0F
#191:1
#  Exit
05 BF 00 01 01 00
#192:0
#  Hint 3
05 C0 00 00 04 03
#192:0
#  OpenChest 16
05 C0 00 00 07 10
#192:1
#  Exit
05 C0 00 01 01 00
#193:0
#  Hint 3
05 C1 00 00 04 03
#193:0
#  OpenChest 17
05 C1 00 00 07 11
#193:1
#  Exit
05 C1 00 01 01 00
#194:0
#  Hint 3
05 C2 00 00 04 03
#194:0
#  OpenChest 18
05 C2 00 00 07 12
#194:1
#  Exit
05 C2 00 01 01 00
#195:0
#  Hint 3
05 C3 00 00 04 03
#195:0
#  OpenChest 19
05 C3 00 00 07 13
#195:1
#  Exit
05 C3 00 01 01 00
#196:0
#  Hint 16
05 C4 00 00 04 10
#196:0
#  StatusText 18
08 C4 00 00 1D 12 00 00 00
#197:0
#  Hint 16
05 C5 00 00 04 10
#197:0
#  StatusText 18
08 C5 00 00 1D 12 00 00 00
#198:0
#  Hint 16
05 C6 00 00 04 10
#198:0
#  StatusText 18
08 C6 00 00 1D 12 00 00 00
#199:0
#  Hint 10
05 C7 00 00 04 0A
#199:0
#  StatusText 11
08 C7 00 00 1D 0B 00 00 00
#200:0
#  Hint 12
05 C8 00 00 04 0C
#200:0
#  Cmp MapVar14 1
0B C8 00 00 0E 8B 00 01 00 00 00 0B
#200:1
#  RandomGoTo
0A C8 00 01 19 02 04 06 08 02 02
#200:2
#  Add Inventory 1488
0A C8 00 02 10 11 00 D0 05 00 00
#200:3
#  GoTo
05 C8 00 03 24 09
#200:4
#  Add Inventory 1489
0A C8 00 04 10 11 00 D1 05 00 00
#200:5
#  GoTo
05 C8 00 05 24 09
#200:6
#  DamagePlayer 5 0 50
0A C8 00 06 09 05 00 32 00 00 00
#200:7
#  StatusText 13
08 C8 00 07 1D 0D 00 00 00
#200:8
#  Add Inventory 1490
0A C8 00 08 10 11 00 D2 05 00 00
#200:9
#  Set MapVar14 1
0A C8 00 09 12 8B 00 01 00 00 00
#200:10
#  SetTexture 2 cwb1
0D C8 00 0A 0B 02 00 00 00 63 77 62 31 00
#200:11
#  Exit
05 C8 00 0B 01 00
#200:12
#  OnLoadMap
05 C8 00 0C 25 00
#200:13
#  Cmp MapVar14 1
0B C8 00 0D 0E 8B 00 01 00 00 00 0F
#200:14
#  GoTo
05 C8 00 0E 24 0B
#200:15
#  SetTexture 2 cwb1
0D C8 00 0F 0B 02 00 00 00 63 77 62 31 00
#201:0
#  Hint 12
05 C9 00 00 04 0C
#201:0
#  Cmp MapVar15 1
0B C9 00 00 0E 8C 00 01 00 00 00 0B
#201:1
#  RandomGoTo
0A C9 00 01 19 02 04 06 08 02 02
#201:2
#  Add Inventory 1488
0A C9 00 02 10 11 00 D0 05 00 00
#201:3
#  GoTo
05 C9 00 03 24 09
#201:4
#  Add Inventory 1489
0A C9 00 04 10 11 00 D1 05 00 00
#201:5
#  GoTo
05 C9 00 05 24 09
#201:6
#  DamagePlayer 5 0 50
0A C9 00 06 09 05 00 32 00 00 00
#201:7
#  StatusText 13
08 C9 00 07 1D 0D 00 00 00
#201:8
#  Add Inventory 1490
0A C9 00 08 10 11 00 D2 05 00 00
#201:9
#  Set MapVar15 1
0A C9 00 09 12 8C 00 01 00 00 00
#201:10
#  SetTexture 3 cwb1
0D C9 00 0A 0B 03 00 00 00 63 77 62 31 00
#201:11
#  Exit
05 C9 00 0B 01 00
#201:12
#  OnLoadMap
05 C9 00 0C 25 00
#201:13
#  Cmp MapVar15 1
0B C9 00 0D 0E 8C 00 01 00 00 00 0F
#201:14
#  GoTo
05 C9 00 0E 24 0B
#201:15
#  SetTexture 3 cwb1
0D C9 00 0F 0B 03 00 00 00 63 77 62 31 00
#415:0
#  Hint 50
05 9F 01 00 04 32
#415:0
#  Cmp QBits 689
0B 9F 01 00 0E 10 00 B1 02 00 00 05
#415:1
#  StatusText 51
08 9F 01 01 1D 33 00 00 00
#415:2
#  Add AutonoteBits 322
0A 9F 01 02 10 E1 00 42 01 00 00
#415:3
#  ForPlayer 5
05 9F 01 03 23 05
#415:4
#  Add QBits 689
0A 9F 01 04 10 10 00 B1 02 00 00
#415:5
#  Exit
05 9F 01 05 01 00
#416:0
#  Hint 20
05 A0 01 00 04 14
#416:0
#  Cmp Awards 104
0B A0 01 00 0E 0C 00 68 00 00 00 04
#416:1
#  FaceAnimation 7 18
06 A0 01 01 3B 07 12
#416:2
#  StatusText 19
08 A0 01 02 1D 13 00 00 00
#416:3
#  Exit
05 A0 01 03 01 00
#416:4
#  EnterHouse 216
08 A0 01 04 02 D8 00 00 00
#417:0
#  Hint 100
05 A1 01 00 04 64
#417:0
#  EnterHouse 17
08 A1 01 00 02 11 00 00 00
#418:0
#  Hint 100
05 A2 01 00 04 64
#418:0
#  Exit
05 A2 01 00 01 00
#418:1
#  EnterHouse 17
08 A2 01 01 02 11 00 00 00
#419:0
#  Hint 100
05 A3 01 00 04 64
#419:0
#  EnterHouse 57
08 A3 01 00 02 39 00 00 00
#420:0
#  Hint 100
05 A4 01 00 04 64
#420:0
#  Exit
05 A4 01 00 01 00
#420:1
#  EnterHouse 57
08 A4 01 01 02 39 00 00 00
#421:0
#  Hint 100
05 A5 01 00 04 64
#421:0
#  EnterHouse 93
08 A5 01 00 02 5D 00 00 00
#422:0
#  Hint 100
05 A6 01 00 04 64
#422:0
#  Exit
05 A6 01 00 01 00
#422:1
#  EnterHouse 93
08 A6 01 01 02 5D 00 00 00
#423:0
#  Hint 100
05 A7 01 00 04 64
#423:0
#  EnterHouse 124
08 A7 01 00 02 7C 00 00 00
#424:0
#  Hint 100
05 A8 01 00 04 64
#424:0
#  Exit
05 A8 01 00 01 00
#424:1
#  EnterHouse 124
08 A8 01 01 02 7C 00 00 00
#425:0
#  Hint 100
05 A9 01 00 04 64
#425:0
#  EnterHouse 321
08 A9 01 00 02 41 01 00 00
#426:0
#  Hint 100
05 AA 01 00 04 64
#426:0
#  Exit
05 AA 01 00 01 00
#426:1
#  EnterHouse 321
08 AA 01 01 02 41 01 00 00
#427:0
#  Hint 100
05 AB 01 00 04 64
#427:0
#  EnterHouse 1579
08 AB 01 00 02 2B 06 00 00
#428:0
#  Hint 100
05 AC 01 00 04 64
#428:0
#  Exit
05 AC 01 00 01 00
#428:1
#  EnterHouse 1579
08 AC 01 01 02 2B 06 00 00
#429:0
#  Hint 100
05 AD 01 00 04 64
#429:0
#  EnterHouse 252
08 AD 01 00 02 FC 00 00 00
#430:0
#  Hint 100
05 AE 01 00 04 64
#430:0
#  Exit
05 AE 01 00 01 00
#430:1
#  EnterHouse 252
08 AE 01 01 02 FC 00 00 00
#431:0
#  Hint 100
05 AF 01 00 04 64
#431:0
#  EnterHouse 293
08 AF 01 00 02 25 01 00 00
#432:0
#  Hint 100
05 B0 01 00 04 64
#432:0
#  Exit
05 B0 01 00 01 00
#432:1
#  EnterHouse 293
08 B0 01 01 02 25 01 00 00
#433:0
#  Hint 100
05 B1 01 00 04 64
#433:0
#  EnterHouse 148
08 B1 01 00 02 94 00 00 00
#434:0
#  Hint 100
05 B2 01 00 04 64
#434:0
#  Exit
05 B2 01 00 01 00
#434:1
#  EnterHouse 148
08 B2 01 01 02 94 00 00 00
#435:0
#  Hint 7
05 B3 01 00 04 07
#435:0
#  EnterHouse 1051
08 B3 01 00 02 1B 04 00 00
#436:0
#  Hint 7
05 B4 01 00 04 07
#436:0
#  EnterHouse 1052
08 B4 01 00 02 1C 04 00 00
#437:0
#  Hint 7
05 B5 01 00 04 07
#437:0
#  EnterHouse 1053
08 B5 01 00 02 1D 04 00 00
#438:0
#  Hint 7
05 B6 01 00 04 07
#438:0
#  EnterHouse 1054
08 B6 01 00 02 1E 04 00 00
#439:0
#  Hint 7
05 B7 01 00 04 07
#439:0
#  EnterHouse 1055
08 B7 01 00 02 1F 04 00 00
#440:0
#  Hint 7
05 B8 01 00 04 07
#440:0
#  EnterHouse 1056
08 B8 01 00 02 20 04 00 00
#441:0
#  Hint 7
05 B9 01 00 04 07
#441:0
#  EnterHouse 1057
08 B9 01 00 02 21 04 00 00
#442:0
#  Hint 7
05 BA 01 00 04 07
#442:0
#  EnterHouse 1058
08 BA 01 00 02 22 04 00 00
#443:0
#  Hint 7
05 BB 01 00 04 07
#443:0
#  EnterHouse 216
08 BB 01 00 02 D8 00 00 00
#444:0
#  Hint 9
05 BC 01 00 04 09
#444:0
#  Exit
05 BC 01 00 01 00
#451:0
#  Hint 100
05 C3 01 00 04 64
#451:0
#  Cmp Invisible 0
0B C3 01 00 0E 3C 01 00 00 00 00 04
#451:1
#  Cmp MapVar4 1
0B C3 01 01 0E 81 00 01 00 00 00 04
#451:2
#  SpeakNPC 780
08 C3 01 02 16 0C 03 00 00
#451:3
#  Set MapVar4 1
0A C3 01 03 12 81 00 01 00 00 00
#451:4
#  Exit
05 C3 01 04 01 00
#452:0
#  Hint 100
05 C4 01 00 04 64
#452:0
#  Cmp Invisible 0
0B C4 01 00 0E 3C 01 00 00 00 00 05
#452:1
#  Cmp MapVar4 2
0B C4 01 01 0E 81 00 02 00 00 00 05
#452:2
#  SetMonGroupBit 56 0x80000 1
0D C4 01 02 39 38 00 00 00 00 00 08 00 01
#452:3
#  SetMonGroupBit 55 0x80000 1
0D C4 01 03 39 37 00 00 00 00 00 08 00 01
#452:4
#  Set MapVar4 2
0A C4 01 04 12 81 00 02 00 00 00
#452:5
#  Exit
05 C4 01 05 01 00
#453:0
#  Hint 100
05 C5 01 00 04 64
#453:0
#  Cmp Invisible 0
0B C5 01 00 0E 3C 01 00 00 00 00 03
#453:1
#  Cmp MapVar4 2
0B C5 01 01 0E 81 00 02 00 00 00 03
#453:2
#  Set MapVar4 0
0A C5 01 02 12 81 00 00 00 00 00
#453:3
#  Exit
05 C5 01 03 01 00
#501:0
#  Hint 2
05 F5 01 00 04 02
#501:0
#  MoveToMap -2384 3064 2091 0 out11.odm
29 F5 01 00 06 B0 F6 FF FF F8 0B 00 00 2B 08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 08 6F 75 74 31 31 2E 6F 64 6D 00
#502:0
#  Hint 2
05 F6 01 00 04 02
#502:0
#  MoveToMap 522 -808 1 0 7d35.blv
28 F6 01 00 06 0A 02 00 00 D8 FC FF FF 01 00 00 00 00 04 00 00 00 00 00 00 00 00 00 00 00 00 03 37 64 33 35 2E 62 6C 76 00
