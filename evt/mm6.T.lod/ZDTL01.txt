#1:0
#  Hint 1
05 01 00 00 04 01
#1:0
#  SetDoorState 1 1
06 01 00 00 0F 01 01
#2:0
#  Hint 1
05 02 00 00 04 01
#2:0
#  SetDoorState 2 1
06 02 00 00 0F 02 01
#3:0
#  Hint 3
05 03 00 00 04 03
#3:0
#  SetDoorState 3 1
06 03 00 00 0F 03 01
#4:0
#  Hint 1
05 04 00 00 04 01
#4:0
#  SetDoorState 4 1
06 04 00 00 0F 04 01
#5:0
#  Hint 2
05 05 00 00 04 02
#5:0
#  OpenChest 1
05 05 00 00 07 01
#6:0
#  Hint 4
05 06 00 00 04 04
#6:0
#  OpenChest 0
05 06 00 00 07 00
